package com.store.storeback.facade;

import java.util.ArrayList; 
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.store.storeback.dao.ProductDao;
import com.store.storeback.data.ProductData;
import com.store.storeback.data.TagData;
import com.store.storeback.model.ProductModel;
import com.store.storeback.model.Tag;
/**
 * 
 * @author igor.leonardo.silva
 *
 */
@Component
public class ProductFacade {
	@Autowired
	private ProductDao productDao;
	
	public List<ProductData> getProducts(){ 
		return converterList(productDao.getProducts());
	} 
	/**
	 * 
	 * @param list
	 * @return
	 */
	private List<ProductData> converterList(List<ProductModel> list){
		List<ProductData> listData = new ArrayList<>();
		list.stream().forEach(p -> listData.add(converProduct(p))); 
		return listData;
	}
	/**
	 * 
	 * @param p
	 * @return
	 */
	private ProductData converProduct(ProductModel p){
		ProductData pData = new ProductData();
		pData.setId(p.getId());
		pData.setName(p.getName());
		pData.setTags(convertListTag(p.getTags()));
		return pData;	
	}
	/**
	 * 
	 * @param t
	 * @return
	 */
	private TagData convertTag(Tag t){
		TagData tdata = new TagData();
		tdata.setName(t.getName());		
		return tdata;	
	}
    /**
     * 
     * @param l
     * @return
     */
	private List<TagData> convertListTag(List<Tag> l){
		List<TagData> listData = new ArrayList<>();
		l.stream().forEach(t -> listData.add(convertTag(t))); 
		return listData;
	}
	/**
	 * 
	 * @return
	 */
	public List<TagData> getTags(){
		return convertListTag(productDao.getTags());
	}  
	/**
	 * 
	 * @param pm
	 * @return
	 */
	public Boolean save(ProductData pm){
		ProductModel pdata = new ProductModel(pm.getName(),convertDataToModelList(pm)); 
		return productDao.save(pdata);		
	}
	
	/***
	 *  
	 * @param pm
	 * @return
	 */
	private List<Tag> convertDataToModelList(ProductData pm){
		List<Tag> list = new ArrayList<>();
		for(TagData td: pm.getTags()){
			Tag tm = new Tag(td.getName());
			list.add(tm);
		}  
		return list;
	}
	 
	/**
	 * 
	 * @return
	 */
	public List<ProductData> productList(){  
		return converterList(productDao.productList());
	}
	
	
	/**
	 * 
	 * @return
	 */
	public ProductData getProducById(Integer id){  
		return converProduct(productDao.getProducById(id));
	}
	
}
