/**
 * 
 */
package com.store.storeback.controller;

import java.util.ArrayList;
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RestController; 
import com.store.storeback.data.ProductData;
import com.store.storeback.data.ProductWsdto;
import com.store.storeback.data.ResponseData;
import com.store.storeback.data.TagData; 
import com.store.storeback.facade.ProductFacade;

/**
 * @author igor.leonardo.silva
 *
 */
@RestController
public class StoreController { 

	@Autowired
	private ProductFacade productFacade; 
	
	 /**
     * 
     * @return
     */
	@GetMapping("/productList")
	List<ProductWsdto> productList() { 
		List<ProductData> pList = productFacade.productList(); 
		List<ProductWsdto> dtoList = new ArrayList<ProductWsdto>();
		for(ProductData p: pList){
			ProductWsdto productWsdto = new ProductWsdto();
			productWsdto.setId(p.getId());
			productWsdto.setName(p.getName());
			String tags[] = new String[p.getTags().size()];
			int i = 0;
			for (TagData t : p.getTags()) {
				tags[i] = t.getName();
				i++;
			} 
			productWsdto.setTag(tags);
			productWsdto.setTagsVector(mergeVector(p.getTags()));
			dtoList.add(productWsdto);
		}   
		return dtoList;
	}
	
	
    /**
     * 
     * @return
     */
	@GetMapping("/product")
	ProductWsdto products() {
		ProductData p = productFacade.getProducts().iterator().next();
		ProductWsdto productWsdto = new ProductWsdto();
		productWsdto.setId(p.getId());
		productWsdto.setName(p.getName());
		String tags[] = new String[p.getTags().size()];
		int i = 0;
		for (TagData t : p.getTags()) {
			tags[i] = t.getName();
			i++;
		}
		
		productWsdto.setTag(tags);
		productWsdto.setTagsVector(mergeVector(p.getTags()));
		return productWsdto;
	}
	/**
	 * 
	 * @param listTag
	 * @return
	 */
	private Integer[] mergeVector(List<TagData> listTag){ 
		String vectorTag[] = new String[productFacade.getTags().size()];
		Integer tagsVector[] = new Integer[productFacade.getTags().size()];
		int i = 0;
		for (TagData t : productFacade.getTags()) {
			vectorTag[i] = t.getName();
			i++;
		} 
		
		for(int x = 0 ; x <vectorTag.length; x++ ){
			if(findObject(listTag,vectorTag[x])){
				   tagsVector[x] = new Integer("1");
			}else{
				   tagsVector[x] = new Integer("0");
			} 
		}  
		return tagsVector;
	}

 	private Boolean findObject(List<TagData> listTag,String obj){
		for(TagData data: listTag){
			if(obj.equalsIgnoreCase(data.getName())){
				return true;
			} 
		}
		return false;
	}
	 
 	/**
 	 * 
 	 * @param productWsdto
 	 * @return
 	 */
	@PostMapping(path = "/createProduct", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseData createProduct(@RequestBody ProductWsdto productWsdto) { 
		ResponseData response = new ResponseData();  
		ProductData pdata = convertWsdtoData(productWsdto); 
		productFacade.save(pdata);
		response.setDescricao("Product salvo com sucesso");
		response.setStatus("OK");
		response.setProductWsdto(productWsdto);  
		System.out.print(productWsdto.toString());
		return response;
	}
	
	/**
 	 * 
 	 * @param productWsdto
 	 * @return
 	 */
	@GetMapping("/getProduct/{id}")
	public ResponseData getProduct(@PathVariable Integer id) {  
		ProductData p = productFacade.getProducById(id);
		ProductWsdto productWsdto = new ProductWsdto();
		productWsdto.setId(p.getId());
		productWsdto.setName(p.getName()); 
		ResponseData response = new ResponseData();    
		response.setDescricao("Resultado");
		response.setStatus("OK");
		String tags[] = new String[p.getTags().size()];
		int i = 0;
		for (TagData t : p.getTags()) {
			tags[i] = t.getName();
			i++;
		} 
		productWsdto.setTag(tags);
		productWsdto.setTagsVector(mergeVector(p.getTags()));
		response.setProductWsdto(productWsdto);  
		System.out.print(productWsdto.toString());
		return response;
	}
	
	/**
	 *  
	 * @return
	 */
	private ProductData convertWsdtoData(ProductWsdto productWsdtos){
		ProductData data = new ProductData();
		data.setName(productWsdtos.getName());
		data.setTags(convertVectorInList(productWsdtos));  
		return data;
	}
	/**
	 * 
	 * @param productWsdtos
	 * @return
	 */
	private List<TagData> convertVectorInList(ProductWsdto productWsdtos){
		List<TagData> list = new ArrayList<>(); 
		String vectorTag[] = productWsdtos.getTag(); 
		int i = 0;
		for (String t : vectorTag) {
			 TagData tag = new TagData();
			 tag.setName(t);
			 list.add(tag);
		}  
		return list;
	}

}
