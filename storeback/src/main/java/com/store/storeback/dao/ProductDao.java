package com.store.storeback.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;  
import org.springframework.stereotype.Component; 
import com.store.storeback.model.ProductModel;
import com.store.storeback.model.Tag;
/**
 * 
 * @author igor.leonardo.silva
 *
 */
@Component
public class ProductDao {   
	
	static List<ProductModel> list;
	
	/**
	 * 
	 * @return
	 */
	public ProductModel getProducById(Integer id){
		ProductModel pp = new ProductModel();
		for(ProductModel p: list) {
			if(p.getId().equals(id)){
				pp = p;
				break;
			} 
		}  
		return pp;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public List<ProductModel> productList(){
		return this.list;
	}
	/**
	 * 
	 * @return
	 */
	public List<ProductModel> getProducts(){
		List<Tag> tagList = Arrays.asList(new Tag("balada"),new Tag("metal"),new Tag("delicado"),new Tag("descolado")); 
		List<ProductModel> list = Arrays.asList(new ProductModel(58345,"Saia Maxi Chiffon Saint",tagList));  
		return list;
	} 
	/**
	 * 
	 * @return
	 */
	public List<Tag> getTags(){
		List<Tag> tagList = Arrays.asList(
				new Tag("neutro"),
				new Tag("veludo"),
				new Tag("couro"),
				new Tag("basics"),
				new Tag("festa"),
				new Tag("workwear"),
				new Tag("inverno"),
				new Tag("boho"),
				new Tag("estampas"),
				new Tag("balada"),
				new Tag("colorido"),
				new Tag("casual"),
				new Tag("liso"),
				new Tag("moderno"),
				new Tag("passeio"),
				new Tag("metal"),
				new Tag("viagem"),
				new Tag("delicado"),
				new Tag("descolado"),
				new Tag("elastano")
				);  
		return tagList;		
	}
	
	
	/**
	 * 
	 * @param pm
	 * @return
	 */
	public Boolean save(ProductModel pm){
		if(null == list){
			list = new ArrayList<>();
		} 
		Integer id = list.size();
		if(id >0){
			ProductModel p = list.get(id-1);
			pm.setId(p.getId()+1);	
		}else{
			pm.setId(1);	
		} 
		list.add(pm);
		return true;		
	}
}
