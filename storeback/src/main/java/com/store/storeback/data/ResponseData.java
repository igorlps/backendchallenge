package com.store.storeback.data;

public class ResponseData {
	
	String descricao;
	String status; 
	ProductWsdto productWsdto;
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the productWsdto
	 */
	public ProductWsdto getProductWsdto() {
		return productWsdto;
	}
	/**
	 * @param productWsdto the productWsdto to set
	 */
	public void setProductWsdto(ProductWsdto productWsdto) {
		this.productWsdto = productWsdto;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseData [descricao=" + descricao + ", status=" + status + ", productWsdto=" + productWsdto + "]";
	}
 
	
	

}
