package com.store.storeback.data;

import java.util.Arrays;

public class ProductWsdto {
	@Override
	public String toString() {
		return "ProductWsdto [id=" + id + ", name=" + name + ", tag=" + Arrays.toString(tag) + ", tagsVector="
				+ Arrays.toString(tagsVector) + "]";
	}
	Integer id;
	String name;
	String[] tag;
	Integer[] tagsVector;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the tag
	 */
	public String[] getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String[] tag) {
		this.tag = tag;
	}
	/**
	 * @return the tagsVector
	 */
	public Integer[] getTagsVector() {
		return tagsVector;
	}
	/**
	 * @param tagsVector the tagsVector to set
	 */
	public void setTagsVector(Integer[] tagsVector) {
		this.tagsVector = tagsVector;
	}
	
	 
	

}
