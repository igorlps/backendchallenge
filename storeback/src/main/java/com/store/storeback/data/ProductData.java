/**
 * 
 */
package com.store.storeback.data;

import java.util.List;

import com.store.storeback.model.Tag;

/**
 * @author igor.leonardo.silva
 *
 */
public class ProductData {
	Integer id ;//, which is an integer and serves as the unique identifier of the product
	String name;//, which is a string with the product name
	List<TagData> tags;//, which is an array of strings with product characteristics
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the tags
	 */
	public List<TagData> getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<TagData> tags) {
		this.tags = tags;
	}
 
	
}
