/**
 * 
 */
package com.store.storeback.model;

/**
 * @author igor.leonardo.silva
 *
 */
public class Tag {
String name;



public Tag(String name) {
	super();
	this.name = name;
}

/**
 * @return the name
 */
public String getName() {
	return name;
}

/**
 * @param name the name to set
 */
public void setName(String name) {
	this.name = name;
} 

}
