/**
 * 
 */
package com.store.storeback.model;

import java.util.List;

/**
 * @author igor.leonardo.silva
 *
 */
public class ProductModel {
	
	public Integer id ;//, which is an integer and serves as the unique identifier of the product
	public String name;//, which is a string with the product name
	public List<Tag> tags;//, which is an array of strings with product characteristics  
	
	public ProductModel( ) {
		super();  
	}
	
	public ProductModel( String name, List<Tag> tags) {
		super(); 
		this.name = name;
		this.tags = tags;
	}
	 
	public ProductModel(Integer id, String name, List<Tag> tags) {
		super();
		this.id = id;
		this.name = name;
		this.tags = tags;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the tags
	 */
	public List<Tag> getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
 
	
	
	

}
